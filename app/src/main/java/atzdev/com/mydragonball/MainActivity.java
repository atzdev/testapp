package atzdev.com.mydragonball;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import atzdev.com.mydragonball.R;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    final static String TAG = "myBall";
    private SensorManager sensorMgr;
    private Sensor sensor;
    private float xPos, xAccel, xVel = 0.0f;
    private float yPos, yAccel, yVel = 0.0f;
    private float xMax, yMax;
    private Bitmap mBall;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set only portrait oriientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // no title bar
        //requestWindowFeature(Window.FEATURE_NO_TITLE);

        //on screen always
        getWindow().setFlags(0xFFFFFFFF,
                WindowManager.LayoutParams.FLAG_FULLSCREEN|WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //instance object on screen
        BallCanvas ball = new BallCanvas(MainActivity.this);
        setContentView(ball);

        //check screen maxX , maxY for don't out of screen
        Display disp = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        disp.getSize(size);
        xMax = (float) size.x - 160;
        yMax = (float) size.y - 160;

        Log.d(TAG,"size.x: "+size.x+" /size.y: "+size.y);
        //use sensor
        sensorMgr  = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    // sensor check frequency


    @Override
    protected void onResume() {
        super.onResume();
        if(sensorMgr != null) {
            sensorMgr.registerListener(this,sensor,sensorMgr.SENSOR_DELAY_GAME);
        } else {
            Toast.makeText(this, "No Accelerometer", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(sensor != null) {
            sensorMgr.unregisterListener(this,sensor);
        }
    }

        // send data from sensor


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        xAccel = sensorEvent.values[0];
        yAccel = sensorEvent.values[1];

        updateBall();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // ...
    }

    //update position object
    private void updateBall(){
        float ftFactor = 0.8f;
        xVel += (xAccel * ftFactor);
        yVel += (yAccel * ftFactor);

        float xS = (xVel / 2) * ftFactor;
        float yS = (yVel / 2) * ftFactor;

        //Log.d(TAG,"xS yS"+xS+" "+yS);

        //check target object is holed ?
        if(((xPos >= xMax/2-100) && (xPos <= xMax/2 -30)) && ((yPos >= yMax/2 -100) && (yPos <= yMax /2 -30)) ) {
            Log.d("myBall","hit target xMax: "+xMax+" xPos: "+xPos+ " yMax: "+yMax+" yPos: "+yPos);
//            Toast.makeText(this, "Game Over..", Toast.LENGTH_SHORT).show();

            // cancel sensor system
            /*if(sensor != null) {
                sensorMgr.unregisterListener(this,sensor);
            }*/
        }

        xPos -= xS;
        yPos += yS;

        // if object out of screen reset and control to on screen
        if(xPos>xMax){
            xPos = xMax; xVel =0;

        } else if (xPos < 0) {
            xPos = 0; xVel = 0;
        }
        if (yPos>yMax) {
            yPos = yMax; yVel =0;

        } else if (yPos <0) {
            yPos = 0; yVel = 0;
        }
    }
    public class BallCanvas extends View {
        public BallCanvas(Context context) {
            super(context);
            if(mBall ==null){
                Bitmap ballSrc = BitmapFactory.decodeResource(
                  getResources(),R.drawable.ball01);

                final int w = 160;
                final int h = 160;
                mBall = Bitmap.createScaledBitmap(ballSrc,w,h,true);
            }
        }

        @Override
        protected void onDraw(Canvas canvas) {
            Paint p = new Paint();
            p.setColor(Color.CYAN);
            canvas.drawCircle(xMax/2,yMax/2,90,p);
            canvas.drawBitmap(mBall,xPos,yPos,null);
            invalidate();
        }
    }
}
